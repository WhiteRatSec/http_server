Simple HTTP server on C

The program takes pages (html, css, js) from the `site` folder and sends it to the user.

`make debug`
compiles the program in DEBUG mode with outputting log information.

`make release`
compiles the program in NORMAL mode without outputting log information.

Server uses port 80, so run `./bin/server` with root privilege.
(e.g. `sudo ./bin/server` or `su -c "./bin/server"`)

You can access the pages using the browser at the addresses `localhost` or `127.0.0.1`

Over time, there will be improvements.