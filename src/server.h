#ifndef _SERVER_H_
#define _SERVER_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define BUFFER_SIZE 4096
#define PORT 80

#define SERVER_RESPONSE 		\
	"HTTP/1.1 200 OK\n" 		\
	"Content-Type: text/html\n" \
	"Connection: close\n"		\
	"\n"

// colors
#define RESET 	"\033[0m"

#define RED		"\033[01;31m"
#define GREEN	"\033[01;32m"
#define BLUE 	"\033[01;34m"
#define YELLOW 	"\033[01;33m"

#ifdef DEBUG
	#define log_info(M, ...) fprintf(stdout, GREEN "[INFO] (%s:%d) "\
		M RESET "\n", __FILE__, __LINE__, ##__VA_ARGS__)

	#define log_error() \
	{ \
		fprintf(stderr, RED "[ERROR] (%s:%d) <%i> %s" RESET "\n", \
					__FILE__, __LINE__, errno, strerror(errno)); \
		exit(errno); \
	}

#else
	#define log_info(M, ...)
	#define log_error() \
	{ \
		fprintf(stderr, RED "[ERROR] <%i> %s" RESET "\n", errno, \
												strerror(errno)); \
		exit(errno);\
	}

#endif

extern int errno;

struct sockaddr_in address;

int server_fd;
int client_fd;
int opt;
int address_len;

void init(void);
void start(void);
void stop(void);
void error(char *msg);
void response(void);
void request(void);
void getURL(void);
void getPage(void);
void sendPage(char *msg);

#endif //_SERVER_H_
